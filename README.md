# libhighlight

> Source code highlight library in C

## Introduction

A library for syntax highlighting which supports terminal and html output. Fast and lightweight, has no dependencies.

## Features

  * No dependencies
  * Entirely written in C
  * Fast, if you have something faster - please share it with me

## Supported languages

Currently `libhighlight` supports:

  * C
  * sh
  * perl

## How to Install

The `html` support is optional, so you have to provide a special option for
the configuration script to enable its support.

```
./bootstrap
./configure --enable-html
make
make install
```

## Todo

  * Check how `javascript` highlighting works, especially `ES5`
  * Make highlight for numbers optional

## License

GPLv2

```

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 Author: g0tsu
 Email:  g0tsu at dnmx.0rg
```
