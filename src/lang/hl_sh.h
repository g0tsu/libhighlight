/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  Author: g0tsu
 *  Email:  g0tsu at dnmx.0rg
 */

const int HL_SH_EXPR_SIZ = 12;
const char *HL_SH_EXPR[] = {
  "done",
  "echo",
  "printf",
  "fi",
  "elif",
  "my",
  "use",
  "sub",
  "then",
  "shift",
  "exit",
  "unless"
};

const int HL_SH_TYPE_SIZ = 1;
const char *HL_SH_TYPE[] = {
  "read"
};

const int HL_SH_DECL_SIZ = 3;
const char *HL_SH_DECL[] = {
  "-v",
  "-eq",
  "-n"
};

