/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  Author: g0tsu
 *  Email:  g0tsu at dnmx.0rg
 */

#include <config.h>
#ifdef CONFIG_USE_HTML
#include <libhighlight.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

char *test_str[] = {
  "// this is a single line comment",
  "int hello; //world",
  "void hello   (int world, char *p) {\n if ( a==3) { printf(\"hello\"); } \n}\n",
  "#include <stdio.h>\nint main(int ac, char **av) { \n int a = 1; return 0; \n}\n",
  "char *a = \"hello \\\"world\\\"\n 'and bye'\";\n",
  "t",
  "tc",
  "/* \n * multiline \n comment */",
  "char *c = \"hello <wordl>\";\n"
};

int ts_len = 9;

static int compile_html1() {
  size_t rlen;
  hl_root *res;
  char *html;
  char *str = test_str[1];

  assert((res = hl_parser(str, strlen(str), NULL)));
  assert((html = hl_compile_html(res)));
  hl_root_free(res);
  free(html);

  str = test_str[7];
  assert((res = hl_parser(str, strlen(str), NULL)));
  assert((html = hl_compile_html(res)));
  hl_root_free(res);
  free(html);

  str = test_str[8];
  assert((res = hl_parser(str, strlen(str), NULL)));
  assert((html = hl_compile_html(res)));
  hl_root_free(res);
/*  printf("%s\n", html);*/
  free(html);
}

int main() {

  assert(!compile_html1());
  return 0;
}

#endif

