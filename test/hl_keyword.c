/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  Author: g0tsu
 *  Email:  g0tsu at dnmx.0rg
 */

#include <libhighlight.h>
#include <assert.h>

static int hl_expr_test() {

  if (!hl_keyword_expr("break", 5))
    return 1;

  if (hl_keyword_expr("nIii", 4))
    return 1;

  if (!hl_keyword_type("short", 5))
    return 1;

  if (hl_keyword_type("nIii", 4))
    return 1;

  if (!hl_keyword_decl("auto", 4))
    return 1;

  if (hl_keyword_decl("nIii", 4))
    return 1;

  return 0;
}

int main() {

  assert(!hl_expr_test());

  return 0;
}

