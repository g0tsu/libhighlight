/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  Author: g0tsu
 *  Email:  g0tsu at dnmx.0rg
 */

#include <libhighlight.h>
#include <assert.h>
#include <string.h>

int simple_create_free() {
  hl_node * root = hl_node_create();
  assert(root != NULL);
  hl_node_free(root);
  return 0;
}

int simple_insert_free() {
  hl_node *node = hl_node_create();

  node->text = strdup("hello");
  node = hl_node_insert(node);

  node->text = strdup("world12345");
  node = hl_node_insert(node);

  node->text = strdup("hhhhhhhhhhhhhhhh");
  node = hl_node_insert(node);

  while (node->children) {
    node = node->children;
  }

  for (int i = 0; i < 3; i++) {
    node = hl_node_at(node, i);
/*    printf("%s\n", node->text);*/
  }

  hl_node_free(node);

  return 0;
}

/*
 * Kinda simple linked list tester
 */
int main() {

  assert(!simple_create_free());
  assert(!simple_insert_free());

  return 0;
}

