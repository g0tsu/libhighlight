/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  Author: g0tsu
 *  Email:  g0tsu at dnmx.0rg
 */

#include <libhighlight.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

char *test_str[] = {
  "hello\n",
  "char *p = \"this is a quoted string\";",
  "char p = 'h';",
  "// a single line comment\n",
  "# single line definition/comment\n",
  "/*\n * mutiline\n comment \n */\n"
};

int ts_len = 6;
int changeme = 0;

void simple_next_word(char *word, int len, hl_node *root, hl_ctx *ctx) {
  changeme = 1;
}

int main() {
  size_t rlen;
  hl_root *res;

  for (int i = 0; i < ts_len; i++) {
    changeme = 0;
    rlen = strlen(test_str[i]);
    res = hl_parser(test_str[i], rlen, simple_next_word);

/*    printf("rlen: %d, res: %d\n", rlen, res->text_size);*/
    assert(changeme == 1);
    assert(res != NULL);
    assert(res->text_size == rlen);
    hl_root_free(res);
  }

  for (int i = 0; i < ts_len; i++) {
    changeme = 0;
    rlen = strlen(test_str[i]);
    res = hl_parser(test_str[i], rlen, NULL);

/*    printf("rlen: %d, res: %d\n", rlen, res->text_size);*/
    assert(res != NULL);
    assert(res->text_size == rlen);
    hl_root_free(res);
  }

  return 0;
}

