/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  Author: g0tsu
 *  Email:  g0tsu at dnmx.0rg
 */

#include <libhighlight.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

char *test_str[] = {
  "// this is a single line comment",
  "void hello   (int world, char *p) {\n if ( a==3) { printf(\"hello\"); } \n}\n",
  "#include <stdio.h>\nint main(int ac, char **av) { \n int a = 1; return 0; \n}\n",
  "char *a = \"hello \\\"world\\\"\n 'and bye'\";\n",
  "t",
  "tc",
  "int hello; //world",
  "/* \n * multiline \n comment */"
};

int ts_len = 8;

int main() {
  size_t rlen;
  hl_root *res;

  for (int i = 0; i < ts_len; i++) {
    rlen = strlen(test_str[i]);
    res = hl_parser(test_str[i], rlen, NULL);

    hl_compile_pp(res->node);
    printf("\n");

    assert(res != NULL);
    assert(res->text_size == rlen);

    hl_root_free(res);
  }

  return 0;
}

