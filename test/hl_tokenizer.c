/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  Author: g0tsu
 *  Email:  g0tsu at dnmx.0rg
 */

#include <libhighlight.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

int expr_inside_test() {
  hl_root *res;
  const char *str1 = "// void is not int comment";
  const char *str2 = "void stst () {  //comment\n }";
  size_t slen1 = strlen(str1);
  size_t slen2 = strlen(str2);

  assert((res = hl_parser(str1, slen1, NULL)));
  assert(res->node->root->children->type == HL_NODE_COMMENT_START);
  assert(res->node->root->children->type == HL_NODE_COMMENT_START);
  assert(res->node->parent->type == HL_NODE_COMMENT_END);

  // void
  assert(res->node->root->children->children->children->children->text_len == 4);
  assert(res->node->root->children->children->children->children->type != HL_NODE_TYPE);

  hl_root_free(res);

  assert((res = hl_parser(str2, slen2, NULL)));

  assert(res->node->root->children->type == HL_NODE_TYPE);

  hl_root_free(res);

  return 0;
}

int main() {

  assert(!expr_inside_test());

  return 0;
}

